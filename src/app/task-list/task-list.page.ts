import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.page.html',
  styleUrls: ['./task-list.page.scss'],
})
export class TaskListPage implements OnInit {
  userId: any;
  taskList: any = [];
  constructor(private apiService: ApiService) { }

  ngOnInit() {

  }

  getAllTasks() {
    this.apiService.getTasks(this.userId).subscribe((res) => {
      console.log('response from server: ', res);
      this.taskList = res.response;
    }, (err) => {
      console.error('Error while getting task: ', err);
    })
  }

}
