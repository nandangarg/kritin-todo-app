import { Component } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  task = {
    taskName: '',
    description: '',
    uid: '',
    deadline: '',
    status: ''
  }

  constructor(private apiService: ApiService, private router: Router) {}


  /**
   * function for adding new task
   */
  onClickAddTask() {
    console.log('data to be save: ', this.task);
    // calling API function..
    this.apiService.addNewTask(this.task).subscribe((res) => {
      console.log('response from server: ', res);
      console.log('response from server with plus: '+ res);
    }, (err) => {
      console.error('Error while adding task: ', err);
    })

  } // end of onClickAddTask() function...

  /**
   * function for
   */
  goToTaskList() {
    this.router.navigate(['/task-list'])
  }


} // end of class
