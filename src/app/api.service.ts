import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  addNewTask(task): Observable<any> {
    return this.httpClient.post(
      'http://18.188.242.36:7105/addUpdateTask',
       task
    );
  }

  getTasks(uid): Observable<any> {
    return this.httpClient.get('http://18.188.242.36:7105/ShowTask?uid='+uid);
  }
}
